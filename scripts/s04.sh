#!/bin/sh
echo "Example 04"

# Initialize some variables that will be used in the script
IMAGE_NAME="demo/docgen_buildah"
IMAGE_TAG="dev"
BASE_DIR="/tmp/docgen"
OUTPUT_FILE="/tmp/output/oci.pdf"
ADOC_LOCATION="${BASE_DIR}/repo/resources/oci.adoc"

# FROM
doc_ctr=$(buildah from asciidoctor/docker-asciidoctor:1.1.0)

# ENV
buildah config --env OUTPUT_FILE="${OUTPUT_FILE}" $doc_ctr
buildah config --env ADOC_LOCATION="${ADOC_LOCATION}" $doc_ctr

# RUN
buildah run $doc_ctr /bin/sh -c 'apk update'
buildah run $doc_ctr /bin/sh -c 'gem install pygments.rb; \
apk add git \
        gcc \
        python \
        musl-dev;
adduser student -D student'

# USER
buildah config --user student $doc_ctr

# RUN
buildah run $doc_ctr /bin/sh -c "mkdir ${BASE_DIR}"

# WORKDIR
buildah config --workingdir ${BASE_DIR} $doc_ctr

# RUN
buildah run $doc_ctr /bin/sh -c "git clone \
        https://gitlab.com/ivan.mr/buildah-podman-docs.git repo"

# CMD
buildah config --cmd "asciidoctor-pdf -r asciidoctor-diagram \
                    --out-file ${OUTPUT_FILE} \
                    ${ADOC_LOCATION}" $doc_ctr

# Commit the image to end the build process
buildah commit $doc_ctr $IMAGE_NAME:$IMAGE_TAG

# Let's list again the images, now you should see the second generated image
buildah images