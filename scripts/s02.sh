#!/bin/sh
echo "Example 02"

CUSTOM_IMAGE="demo1/nginx"
CONTAINER_NAME="demo_nginx"
# Check the help for any command
podman --help
# List images
podman images
# Let's run our previously built image with buildah
# Run in detached mode and remove the container after exit
# Expose container port (80) in the host (8080)
podman run --rm -d \
	--name ${CONTAINER_NAME} \
	-p 8080:80 \
	${CUSTOM_IMAGE}
# Let's verify the container port mappings
podman port ${CONTAINER_NAME}
# Let's run a GET request to verify our custom index.html is served by nginx
n=0
# Let's introduce a retry mechanism. The container may not be ready right away
until [ "$n" -ge 5 ]
do
	curl http://localhost:8080 && break
	n=$((n+1))
	echo "Retry ${n}..."
	sleep 1s
done
curl http://localhost:8080
# Now stop the container
podman stop ${CONTAINER_NAME}