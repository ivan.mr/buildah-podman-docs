# OCI containers using Buildah and Podman

Please, read the [oci.adoc](resources/oci.adoc) requirements section to
understand required environment to run this repo.

```bash
$ cd env
$ vagrant up
$ vagrant ssh
# Run the lab commands in order
>$ for i in `ls -v`; do bash $i; done;
# Check the generated PDF files with the documentation
# in the folder env/out
```

Once you are done with the environment just exit from the ssh connection and run
```bash
$ vagrant destroy
# It will also automatically unregister the RHEL system
```