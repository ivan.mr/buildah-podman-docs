echo "Configuring environment"

MODULE_NAME="container-tools"

subscription-manager refresh
# Install required sw for lab purposes
yum module list | grep "${MODULE_NAME}" \
	&&  echo "Installing module: ${MODULE_NAME}" \
	||  { echo "Error: ${MODULE_NAME} does not exist" && exit 1; }
yum module install -y $MODULE_NAME
yum install -y tree
# Docker is no longer in RHEL package repositories
dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo

dnf list docker-ce || { echo "Error: docker-ce package not available" && exit 1; }
dnf install docker-ce --nobest -y
systemctl start docker
systemctl enable docker
usermod -aG docker vagrant

echo "==Health-check..."
echo "===Podman check..."
podman --version || { echo "Error: podman not available" && exit 1; }
echo "===Buildah check..."
buildah --version || { echo "Error: buildah not available" && exit 1; }
echo "===Docker check..."
sudo docker version

echo "==Verifying lab files"
find /tmp/lab -type f -print0 | xargs -0 dos2unix
tree /tmp/lab

# Modify default directory to enter into when running vagrant ssh
echo "cd /tmp/lab/scripts" >> /home/vagrant/.bashrc

echo "Lab environment ready!"
