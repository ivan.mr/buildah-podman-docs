#!/bin/sh
echo "Example 01"

CUSTOM_IMAGE="demo1/nginx"
# Get help
buildah --help
# List images
buildah images
# Let's create a simple image
DEMO_CTR=$(buildah from nginx:1.19)
# Showing the avialable images will prompt us with the downloaded nginx image
buildah images
# Inspect the image
buildah inspect $(buildah images | tail -n1 | awk '{ print $3}')
# You will notice that docker cannot find any nginx image
docker images # Will return an empty list
# This is due to how buildah and docker handle images
docker pull nginx && docker images
# Continue working on the image using buildah commands
# Let's add a file to serve rather than default nginx
buildah add $DEMO_CTR '/tmp/lab/resources/index.html' '/usr/share/nginx/html/index.html'
# Let's add a port to expose when running containers based on the image
buildah config --port 80 "$DEMO_CTR"
# Finally let's write the image using the container layers
buildah commit "$DEMO_CTR" "${CUSTOM_IMAGE}"
