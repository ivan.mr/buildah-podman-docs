#!/bin/sh
echo "Example 03"

IMAGE_NAME="demo/docgen"
TAG="dev"
# If the Dockerfile is not in the current location where the command is
# executed, it must be specified
DOCKERFILE="../resources/Dockerfile"
# The context that buildah will receive to perform the build
# It is very relevant, since paths in the Dockerfile are relative to the context
CONTEXT="../resources/"

# 'buildah bud' command is what we need to execute to build using a Dockerfile
buildah bud --no-cache -f ${DOCKERFILE} -t ${IMAGE_NAME}:${TAG} ${CONTEXT}

buildah images