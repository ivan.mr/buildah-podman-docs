.ONESHELL:
SHELL := /bin/bash
ADOC_FILE = "resources/oci.adoc"
OUTPUT_FILE = "out/oci.pdf"

.PHONY: all
all: install build

.PHONY: install
install:
	sudo apt-get update
	sudo apt install asciidoc asciidoctor
	sudo gem install asciidoctor asciidoctor-pdf --pre
	sudo gem install pygments.rb asciidoctor-diagram

.PHONY: build
build:
	asciidoctor-pdf -r asciidoctor-diagram --out-file $(OUTPUT_FILE) $(ADOC_FILE)
